import React from 'react';

const Custom = ({ customs }) => {
    const customsList = customs.map( customs => {
        return customs.age > 20 ? (
            <div className="custom" key={ customs.id }>
                <div>Name: { customs.name }</div>
                <div>Age: { customs.age }</div>
                <div>Belt: { customs.belt }</div>
            </div> 
        ) : null
    })
    return(
        <div className="custom-list">
            { customsList }
        </div>
    )
}

export default Custom;