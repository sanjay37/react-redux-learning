import React, { Component } from 'react';
import Custom from './Custom';
import AddNinja from './AddNinja';

class App extends Component {
   state = {
    customs : [
      { name: 'Ryu', age: 30, belt: 'black', id: 1 },
      { name: 'Yoshi', age: 20, belt: 'green', id: 2 },
      { name: 'Yark', age: 25, belt: 'blue', id: 3 }
    ]
  }

  addNinja = (ninja) => {
    console.log(ninja);
    ninja.id = Math.random();
    let ninjas = [...this.state.customs, ninja];
    this.setState({
      customs: ninjas
    })
  }

  render() {
    return (
      <div className="App">
        <h1>My First React App!</h1>
        <p>Welcome :)</p>
        <Custom customs={this.state.customs} />
        <AddNinja addNinja={this.addNinja} />
      </div>
    );
  }
}

export default App;
